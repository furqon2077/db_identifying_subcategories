
-- Calculating total sales per product subcategory for specified years using a Common Table Expression (CTE)
WITH SubcategorySales AS (
  -- Selecting product subcategory, sales year, and calculating total sales
  SELECT
    p.prod_subcategory,
    EXTRACT(YEAR FROM s.time_id) AS sales_year,
    SUM(s.amount_sold) AS total_sales
  FROM
    sh.products p
    JOIN sh.sales s ON p.prod_id = s.prod_id
  WHERE
    EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2000, 2001)
  GROUP BY
    p.prod_subcategory, sales_year
),
-- Retrieving current year sales and previous year sales per product subcategory
PreviousYearSales AS (
  -- Selecting product subcategory, current year, current year sales,
  -- and using LAG function to retrieve previous year sales
  SELECT
    prod_subcategory,
    sales_year AS current_year,
    total_sales AS current_year_sales,
    LAG(total_sales) OVER (PARTITION BY prod_subcategory ORDER BY sales_year) AS previous_year_sales
  FROM
    SubcategorySales
)
-- Selecting distinct product subcategories where current year sales exceed previous year sales
SELECT DISTINCT
  prod_subcategory
FROM
  PreviousYearSales
WHERE
  current_year_sales > COALESCE(previous_year_sales, 0);